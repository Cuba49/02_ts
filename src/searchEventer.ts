import { Request } from './request';
import { IMovieResponce, IMovie } from './models/movieModel';
import { MovieViewHelper } from './movieViewHelper';
export class SearchEventer {
    request = new Request();
    movieViewHelper = new MovieViewHelper();
    submit = <HTMLElement>document.getElementById('submit');
    search = <HTMLInputElement>document.getElementById('search');
    popular = <HTMLInputElement>document.getElementById('popular');
    upcoming = <HTMLInputElement>document.getElementById('upcoming');
    top_rated = <HTMLInputElement>document.getElementById('top_rated');
    load_more = <HTMLInputElement>document.getElementById('load-more');
    constructor() {
        if (this.submit && this.search) {
            this.submit.onclick = this.onSearch.bind(this);
            this.search.onkeyup = this.onKeyUpSearch.bind(this);
        }
        if (this.popular) {
            this.popular.onclick = this.onGetPopular.bind(this);
        }
        if (this.upcoming) {
            this.upcoming.onclick = this.onGetUpcoming.bind(this);
        }
        if (this.top_rated) {
            this.top_rated.onclick = this.onGetTopRated.bind(this);
        }
        if (this.load_more) {
            this.load_more.onclick = this.onLoadMore.bind(this);
        }
        this.onGetPopular();
        this.onReactionChanged();
    }
    private onKeyUpSearch(ev: KeyboardEvent): void {
        if (ev.code === 'Enter') {
            this.onSearch();
        }
    }
    private AddListenerLikes(): void {
        const likes = document.getElementsByName('movie_like');
        likes.forEach((like) => {
            like.onclick = this.onMovieLike.bind(this);
        });
    }
    private onMovieLike(ev: MouseEvent): void {
        const like = ev.target as HTMLElement;
        this.movieViewHelper.setReaction(like);
        this.onReactionChanged();
    }
    private async onReactionChanged(): Promise<void> {
        const favorites: number[] = JSON.parse(
            localStorage.getItem('favorites_movie') || '[]'
        );
        const likedMovies: Promise<IMovie>[] = [];
        favorites.forEach((id) => {
            const data: Promise<IMovie> = this.request.getMovie(id);
            likedMovies.push(data);
        });
        Promise.all(likedMovies).then((movies) => {
            this.movieViewHelper.renderFavoritesMovie(movies);
            this.AddListenerLikes();
        });
    }
    private async onLoadMore(): Promise<void> {
        const data: IMovieResponce = await this.request.getMovies(
            this.request.lastQuery.query,
            this.request.lastQuery.nextPage
        );
        this.movieViewHelper.setMoviesView(data);
        this.AddListenerLikes();
    }
    private async onGetTopRated(): Promise<void> {
        const data: IMovieResponce = await this.request.getMovies(
            this.request.dictRequest.topRated
        );
        this.movieViewHelper.setMoviesView(data);
        this.AddListenerLikes();
    }
    private async onGetPopular(): Promise<void> {
        const data: IMovieResponce = await this.request.getMovies(
            this.request.dictRequest.popular
        );
        this.movieViewHelper.setMoviesView(data);
        this.AddListenerLikes();
    }
    private async onGetUpcoming(): Promise<void> {
        const data: IMovieResponce = await this.request.getMovies(
            this.request.dictRequest.upcoming
        );
        this.movieViewHelper.setMoviesView(data);
        this.AddListenerLikes();
    }
    private async onSearch(): Promise<void> {
        if (this.search?.value?.length > 0) {
            const data: IMovieResponce = await this.request.getMovies(
                this.request.dictRequest.search + this.search.value
            );
            this.movieViewHelper.setMoviesView(data);
            this.AddListenerLikes();
        }
    }
}
