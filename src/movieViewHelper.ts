import { IMovieResponce, IMovie } from './models/movieModel';
export class MovieViewHelper {
    public setMoviesView(moviesModel: IMovieResponce): void {
        const filmContainer = document.getElementById('film-container');
        if (filmContainer) {
            if (moviesModel.page === 1) {
                filmContainer.innerHTML = '';
            }
            moviesModel.results.forEach((movie) => {
                filmContainer.appendChild(this.renderMovie(movie));
            });
            this.renderRandomMovie(
                moviesModel.results[this.random(moviesModel.results.length)]
            );
        }
    }
    public setReaction(like: HTMLElement): void {
        const isLiked = like.getAttribute('fill') === 'red';
        like.setAttribute('fill', isLiked ? '#ff000078' : 'red');
        const favorites: number[] = JSON.parse(
            localStorage.getItem('favorites_movie') || '[]'
        );
        if (isLiked) {
            const index = favorites.indexOf(Number.parseInt(like.id));
            index !== -1 ? favorites.splice(index, 1) : '';
        } else {
            favorites.push(Number.parseInt(like.id));
        }
        localStorage.setItem('favorites_movie', JSON.stringify(favorites));
    }
    public renderFavoritesMovie(movies: IMovie[]): void {
        const favorites = document.getElementById('favorite-movies');
        if (favorites) {
            favorites.innerHTML = '';
            movies.forEach((movie) => {
                favorites.appendChild(this.renderMovie(movie, true));
            });
        }
    }
    private renderRandomMovie(movie: IMovie): void {
        const randomMovie = document.getElementById('random-movie');
        randomMovie?.setAttribute(
            'style',
            'background-size: cover;background-image:' +
                (movie?.backdrop_path
                    ? 'url(https://image.tmdb.org/t/p/original/' +
                      movie.backdrop_path +
                      ')'
                    : 'none;')
        );
        const randomMovieName = document.getElementById('random-movie-name');
        if (randomMovieName) {
            randomMovieName.innerText = movie?.title;
        }
        const randomMovieDesc = document.getElementById(
            'random-movie-description'
        );
        if (randomMovieDesc) {
            randomMovieDesc.innerText = movie?.overview;
        }
    }
    private random(max: number): number {
        max = Math.floor(max);
        return Math.floor(Math.random() * (max + 1));
    }
    private renderMovie(movie: IMovie, isBig = false): HTMLElement {
        const movieElement = document.createElement('div');
        movieElement.setAttribute(
            'class',
            isBig ? 'col-12 p-2' : 'col-lg-3 col-md-4 col-12 p-2'
        );
        const card = document.createElement('div');
        card.setAttribute('class', 'card shadow-sm');
        const img = document.createElement('img');
        img.setAttribute(
            'src',
            movie.poster_path
                ? 'https://image.tmdb.org/t/p/original/' + movie.poster_path
                : 'https://www.serieslike.com/img/shop_01.png'
        );
        const xmlns = 'http://www.w3.org/2000/svg';
        const svg = document.createElementNS(xmlns, 'svg');
        const favorites: number[] = JSON.parse(
            localStorage.getItem('favorites_movie') || '[]'
        );
        svg.setAttribute('xmlns', xmlns);
        svg.setAttribute('stroke', 'red');
        svg.setAttribute(
            'fill',
            favorites.indexOf(movie.id) === -1 ? '#ff000078' : 'red'
        );
        svg.setAttribute('width', '50');
        svg.setAttribute('height', '50');
        svg.setAttribute('class', 'bi bi-heart-fill position-absolute p-2');
        svg.setAttribute('viewBox', '0 -2 18 22');
        svg.setAttribute('name', 'movie_like');
        const path = document.createElementNS(xmlns, 'path');
        path.setAttribute('fill-rule', 'evenodd');
        path.setAttribute('id', movie.id.toString());
        path.setAttribute(
            'fill',
            favorites.indexOf(movie.id) === -1 ? '#ff000078' : 'red'
        );
        path.setAttribute(
            'd',
            'M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z'
        );
        svg.appendChild(path);
        const cardBody = document.createElement('div');
        cardBody.setAttribute('class', 'card-body');
        const cardText = document.createElement('p');
        cardText.setAttribute('class', 'card-text truncate');
        cardText.innerHTML = movie.overview;
        const bottom = document.createElement('div');
        bottom.setAttribute(
            'class',
            'd-flex justify-content-between align-items-center'
        );
        const small = document.createElement('small');
        small.setAttribute('class', 'text-muted');
        small.innerHTML = movie.release_date;
        bottom.appendChild(small);
        cardBody.appendChild(cardText);
        cardBody.appendChild(bottom);
        card.appendChild(img);
        card.appendChild(svg);
        card.appendChild(cardBody);
        movieElement.appendChild(card);
        return movieElement;
    }
}
