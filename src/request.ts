import {
    IMovieResponce,
    IMovie,
    Movie,
    MovieResponce,
} from './models/movieModel';
export class Request {
    private api_key = '6ded9e43f2117b5d5661cdfd551cb02e';
    private api_language = 'en-US';
    private api_base = `https://api.themoviedb.org/3`;
    public dictRequest = {
        search: '/search/movie?query=',
        popular: '/movie/popular?',
        upcoming: '/movie/upcoming?',
        topRated: '/movie/top_rated?',
        getMovie: '/movie/',
        lastQuery: '',
        lastPage: 1,
    };
    public lastQuery = {
        query: '',
        nextPage: 0,
    };

    public getMovie(id: number): Promise<IMovie> {
        return this.getData<IMovie>(this.dictRequest.getMovie + id + '?').then(
            (movie) => {
                return this.mapperIMovie(movie);
            }
        );
    }
    public getMovies(request: string, page = 1): Promise<IMovieResponce> {
        this.lastQuery.query = request;
        this.lastQuery.nextPage = page + 1;
        return this.getData<IMovieResponce>(request, page).then((responce) => {
            return this.mapperIMovieResponce(responce);
        });
    }
    private async getData<T>(request: string, page?: number): Promise<T> {
        const url =
            this.api_base +
            request +
            '&api_key=' +
            this.api_key +
            '&language=' +
            this.api_language +
            (page ? '&page=' + page : '');
        return fetch(url)
            .then((response) => {
                if (!response.ok) {
                    throw new Error(response.statusText);
                }
                return response.json();
            })
            .then((data) => {
                return data as T;
            });
    }
    private mapperIMovie(movie: IMovie): IMovie {
        return new Movie(
            movie.id,
            movie.title,
            movie.poster_path,
            movie.overview,
            movie.release_date,
            movie.backdrop_path
        );
    }
    private mapperIMovieResponce(data: IMovieResponce): IMovieResponce {
        const movies: Movie[] = [];
        (data as MovieResponce).results.forEach((movie) => {
            movies.push(
                new Movie(
                    movie.id,
                    movie.title,
                    movie.poster_path,
                    movie.overview,
                    movie.release_date,
                    movie.backdrop_path
                )
            );
        });
        return new MovieResponce(movies, data.page, data.total_pages);
    }
    // Не шукайте мене в коді - мене так ніхто і не використовує.
    // Я лише доказ, що мій творець питався...
    private async mapper(
        data: MovieResponce | Movie
    ): Promise<MovieResponce | Movie> {
        if (data instanceof Movie) {
            return new Movie(
                data.id,
                data.title,
                data.poster_path,
                data.overview,
                data.release_date,
                data.backdrop_path
            );
        } else if (data instanceof MovieResponce) {
            const movies: Movie[] = [];
            (data as MovieResponce).results.forEach((movie) => {
                movies.push(
                    new Movie(
                        movie.id,
                        movie.title,
                        movie.poster_path,
                        movie.overview,
                        movie.release_date,
                        movie.backdrop_path
                    )
                );
            });
            return new MovieResponce(movies, data.page, data.total_pages);
        } else {
            throw new Error();
        }
    }
}
