export interface IMovie{
    id: number;
    title: string;
    poster_path: string;
    overview: string;
    release_date: string;
    backdrop_path: string;
}
export class Movie implements IMovie{
    id;
    title;
    poster_path;
    overview;
    release_date;
    backdrop_path;
    constructor(id: number,title: string,poster_path: string,overview: string,release_date: string,backdrop_path: string) {
        this.id = id;
        this.title = title;
        this.poster_path = poster_path;
        this.overview = overview;
        this.release_date = release_date;
        this.backdrop_path = backdrop_path;
    }

}
export interface IMovieResponce{
    results: IMovie[];
    page: number;
    total_pages: number;
}
export class MovieResponce implements IMovieResponce{
    results;
    page;
    total_pages;
    constructor(results: IMovie[],page: number,total_pages: number) {
        this.results = results;
        this.page = page;
        this.total_pages = total_pages;
    }
}
